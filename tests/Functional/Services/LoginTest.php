<?php

namespace Tests\Smorken\R25\Functional\Services;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Smorken\R25\Converters\ToArray;
use Smorken\R25\Requesters\Guzzle;
use Smorken\R25\Services\Login;
use Smorken\R25\Xml\SimpleXml;

class LoginTest extends TestCase
{
    public function testLoginAlreadyLoggedIn(): void
    {
        $sut = new Login($this->getHandler(['login_exists.xml' => 200]));
        $this->assertTrue($sut->login('foo', 'bar'));
    }

    public function testLoginNotLoggedInFails(): void
    {
        $sut = new Login($this->getHandler(['login_challenge.xml' => 200, 'login_response_fail.xml' => 200]));
        $this->assertFalse($sut->login('foo', 'bar'));
    }

    public function testLoginNotLoggedInSuccess(): void
    {
        $sut = new Login($this->getHandler(['login_challenge.xml' => 200, 'login_response.xml' => 200]));
        $this->assertTrue($sut->login('foo', 'bar'));
    }

    public function testNeedsLoginIsFalse(): void
    {
        $sut = new Login($this->getHandler(['login_exists.xml' => 200]));
        $this->assertFalse($sut->needsLogin($sut->getLoginResponse()));
    }

    public function testNeedsLoginIsTrue(): void
    {
        $sut = new Login($this->getHandler(['login_challenge.xml' => 200]));
        $this->assertTrue($sut->needsLogin($sut->getLoginResponse()));
    }

    protected function createResponses(array $endpoints): array
    {
        $responses = [];
        foreach ($endpoints as $endpoint => $status_code) {
            $responses[] = new Response($status_code, [], $this->getXml($endpoint));
        }

        return $responses;
    }

    /**
     * @param  int  $response_code
     * @return Client
     */
    protected function getClient(string|array $endpoint): ClientInterface
    {
        $mock = new MockHandler($this->createResponses($endpoint));
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        return $client;
    }

    /**
     * @param  int  $response_code
     * @return \Smorken\R25\Contracts\Request
     */
    protected function getHandler(string|array $endpoint): Guzzle
    {
        $client = $this->getClient($endpoint);

        return new Guzzle($client, new SimpleXml, new ToArray, ['base_uri' => 'http://foo.com']);
    }

    protected function getXml(string $endpoint): string
    {
        $fp = __DIR__.'/../../data/'.$endpoint;

        return file_get_contents($fp);
    }
}
