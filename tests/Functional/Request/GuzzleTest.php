<?php

namespace Tests\Smorken\R25\Functional\Request;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Smorken\R25\Contracts\Xml;
use Smorken\R25\Converters\ToArray;
use Smorken\R25\Requesters\Guzzle;
use Smorken\R25\Xml\Exception;
use Smorken\R25\Xml\SimpleXml;

class GuzzleTest extends TestCase
{
    public function testBadRequest(): void
    {
        $s = $this->getSUT('bad_request.xml');
        $this->expectException(Exception::class);
        $a = $s->requestAndConvert('GET', 'bad_request.xml');
    }

    public function testConvertReturnsArray(): void
    {
        $s = $this->getSUT('simple.xml');
        $r = $s->request('GET', $s->createUrl('simple.xml'));
        $x = $s->convertToXmlObject($r);
        $this->assertTrue(is_array($s->convert($x)
            ->getRaw()));
    }

    public function testConvertToArray(): void
    {
        $s = $this->getSUT('simple.xml');
        $r = $s->request('GET', $s->createUrl('simple.xml'));
        $x = $s->convertToXmlObject($r);
        $a = $s->convert($x)
            ->toArray();
        $this->assertArrayHasKey('simple', $a);
        $this->assertCount(3, $a['simple']);
    }

    public function testConvertToXmlObjectReturnsXmlContract(): void
    {
        $s = $this->getSUT('simple.xml');
        $r = $s->request('GET', $s->createUrl('simple.xml'));
        $this->assertInstanceOf(Xml::class, $s->convertToXmlObject($r));
    }

    public function testCreateUrl(): void
    {
        $s = $this->getSUT('simple.xml');
        $this->assertEquals('http://foo.com/simple.xml', $s->createUrl('simple.xml'));
    }

    public function testRequestAndConvert(): void
    {
        $s = $this->getSUT('simple.xml');
        $a = $s->requestAndConvert('GET', 'simple.xml')
            ->toArray();
        $this->assertArrayHasKey('simple', $a);
        $this->assertCount(3, $a['simple']);
    }

    public function testRequestReturnsResponseInterface(): void
    {
        $s = $this->getSUT('simple.xml');
        $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $s->request('GET', $s->createUrl('simple.xml')));
    }

    /**
     * @return Client
     */
    protected function getClient(string $endpoint, int $response_code = 200): ClientInterface
    {
        $r = new Response($response_code, [], $this->getXml($endpoint));
        $mock = new MockHandler([
            $r,
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        return $client;
    }

    /**
     * @return \Smorken\R25\Contracts\Request
     */
    protected function getSUT(string $endpoint, int $response_code = 200): Guzzle
    {
        $client = $this->getClient($endpoint);

        return new Guzzle($client, new SimpleXml, new ToArray, ['base_uri' => 'http://foo.com']);
    }

    protected function getXml(string $endpoint): string
    {
        $fp = __DIR__.'/../../data/'.$endpoint;

        return file_get_contents($fp);
    }
}
