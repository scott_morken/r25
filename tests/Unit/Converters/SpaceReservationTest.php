<?php

namespace Tests\Smorken\R25\Unit\Converters;

use PHPUnit\Framework\TestCase;
use Smorken\R25\Contracts\Models\Node;
use Smorken\R25\Converters\ToArray;
use Smorken\R25\Xml\SimpleXml;

class SpaceReservationTest extends TestCase
{
    public function testCreateFromArray(): void
    {
        $m = new ToArray;
        $m->createFromArray($this->getArray());
        $a = $m->getRaw();
        //        var_dump($a);
        $this->assertArrayHasKey('space_reservation', $a);
        $this->assertInstanceOf(Node::class, $a['space_reservation'][0]);
    }

    public function testCreateFromXml(): void
    {
        $xml = $this->getXml('rm_reservations.xml');
        $x = new SimpleXml($xml);
        $m = new ToArray;
        $m->create($x);
        $a = $m->getRaw();
        $this->assertArrayHasKey('space_reservation', $a);
        $this->assertInstanceOf(Node::class, $a['space_reservation'][0]);
    }

    public function testCreateMultipleFromXml(): void
    {
        $xml = $this->getXml('rm_reservations.multiple.xml');
        $x = new SimpleXml($xml);
        $m = new ToArray;
        $m->create($x);
        $a = $m->getRaw();
        $this->assertCount(30, $a['space_reservation']);
    }

    public function testToArray(): void
    {
        $m = new ToArray;
        $m->createFromArray($this->getArray());
        $a = $m->toArray();
        $this->assertArrayHasKey('space_reservation', $a);
        $this->assertEquals('', $a['space_reservation'][0]['value']);
        $this->assertCount(2, $a['space_reservation']);
    }

    protected function getArray(): array
    {
        $xml = $this->getXml('rm_reservations.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());

        return $arr;
    }

    protected function getXml(string $filename): string
    {
        $fp = __DIR__.'/../../data/'.$filename;

        return file_get_contents($fp);
    }
}
