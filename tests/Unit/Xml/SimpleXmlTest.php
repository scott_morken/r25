<?php

namespace Tests\Smorken\R25\Unit\Xml;

use PHPUnit\Framework\TestCase;
use Smorken\R25\Xml\Exception;
use Smorken\R25\Xml\SimpleXml;

class SimpleXmlTest extends TestCase
{
    public function testCreateThrowExceptionWithErrorKey(): void
    {
        $xml = $this->getXml('bad_request.xml');
        $this->expectException(Exception::class);
        $x = new SimpleXml($xml);
    }

    public function testFromArrayComplex(): void
    {
        $xml = $this->getXml('events.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $xmlObj = $x->newXmlObject('r25:events', [
            'r25' => 'http://www.collegenet.com/r25',
            'xl' => 'http://www.w3.org/1999/xlink',
        ]);
        $x->fromArray($arr, $xmlObj);
        $this->assertStringEndsWith('<r25:creation_dt>2013-09-26T09:54:10-07:00</r25:creation_dt></r25:event></r25:events>
', $xmlObj->saveXml());
    }

    public function testFromArrayComplexDefaultNamespaces(): void
    {
        $xml = $this->getXml('events.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $xmlObj = $x->newXmlObject('events');
        $x->fromArray($arr, $xmlObj);
        $this->assertStringEndsWith('<r25:creation_dt>2013-09-26T09:54:10-07:00</r25:creation_dt></r25:event></r25:events>
', $xmlObj->saveXml());
    }

    public function testFromArraySimple(): void
    {
        $xml = $this->getXml('simple.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $xmlObj = $x->newXmlObject('r:simples', [
            'r' => 'http://www.collegenet.com/r25',
            'xl' => 'http://www.w3.org/1999/xlink',
        ]);
        $x->fromArray($arr, $xmlObj);
        $this->assertStringEndsWith('3">457</r:simple_type_id><r:simple_empty/></r:simple></r:simples>
', $xmlObj->saveXml());
    }

    public function testGetAttributes(): void
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $a = $x->getAttributes(null);
        $this->assertArrayHasKey('pubdate', $a);
    }

    public function testGetAttributesComplex(): void
    {
        $xml = $this->getXml('events.xml');
        $x = new SimpleXml($xml);
        $a = $x->getAttributes($x->getImmediateChildren());
        $this->assertEquals(['id' => 'CBJDNTc5NDEz', 'crc' => '00000022', 'status' => 'est'], $a);
    }

    public function testGetBase(): void
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $this->assertEquals('login_challenge', $x->getBase());
    }

    public function testGetChildren(): void
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $c = $x->getChildren($x->getXmlObject(), 'r25');
        $this->assertEquals('login', $c->getName());
    }

    public function testGetImmediateAttributes(): void
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $a = $x->getImmediateAttributes();
        $this->assertArrayHasKey('pubdate', $a);
    }

    public function testGetImmediateChildren(): void
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $c = $x->getImmediateChildren();
        $this->assertEquals('login', $c->getName());
    }

    public function testGetName(): void
    {
        $xml = $this->getXml('login_challenge.xml');
        $x = new SimpleXml($xml);
        $this->assertEquals('login_challenge', $x->getName($x->getXmlObject()));
    }

    public function testToArrayComplex(): void
    {
        $xml = $this->getXml('rm_reservations.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $this->assertArrayHasKey('space_reservation', $arr);
        $this->assertCount(2, $arr['space_reservation']);
        $this->assertArrayHasKey('xl:href', $arr['space_reservation'][0]->getAttributes(null));
    }

    public function testToArraySimple(): void
    {
        $xml = $this->getXml('simple.xml');
        $x = new SimpleXml($xml);
        $arr = $x->toArray($x->getXmlObject());
        $this->assertArrayHasKey('simple', $arr);
        $this->assertCount(3, $arr['simple']);
        $this->assertEquals('SIMPLE 1', $arr['simple'][0]->simple_name);
    }

    protected function getXml(string $filename): string
    {
        $fp = __DIR__.'/../../data/'.$filename;

        return file_get_contents($fp);
    }
}
