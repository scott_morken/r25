<?php

namespace Tests\Smorken\R25\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\R25\Factory;

class ArgConstructor
{
    public $arg1;

    public $arg2;

    public function __construct($arg1, $arg2)
    {
        $this->arg1 = $arg1;
        $this->arg2 = $arg2;
    }
}

class FactoryTest extends TestCase
{
    public function testConstructorWithArgs(): void
    {
        $f = $this->getFactory();
        $o = $f->create('args', 'foo', 'bar');
        $this->assertInstanceOf(ArgConstructor::class, $o);
        $this->assertEquals('foo', $o->arg1);
    }

    public function testConstructorWithArgsNoneProvided(): void
    {
        $f = $this->getFactory();
        $this->expectException(\ArgumentCountError::class);
        $o = $f->create('args');
    }

    public function testSimpleConstructor(): void
    {
        $f = $this->getFactory();
        $this->assertInstanceOf(Simple::class, $f->create('simple'));
    }

    protected function getFactory(): Factory
    {
        return new Factory([
            'simple' => Simple::class,
            'args' => ArgConstructor::class,
        ]);
    }
}

class Simple {}
