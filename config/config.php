<?php

return [
    'client' => [
        'test' => false,
        'base_uri' => env('R25_BASE_URI', 'https://25live.baseurl.com/run'),
        'options' => [
            'cookies' => true,
            'debug' => false,
            'timeout' => 20,
        ],
    ],
    'credentials' => [
        'username' => env('R25_USER', 'user'),
        'password' => env('R25_PASSWORD', 'pw'),
    ],
    'services' => [
        \Smorken\R25\Contracts\Services\Login::class => \Smorken\R25\Services\Login::class,
        \Smorken\R25\Contracts\Services\SpaceReservation::class => \Smorken\R25\Services\SpaceReservation::class,
    ],
    'test_responses' => [],
];
