## R25 helper library

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 8.1+
* PHP XML library
* [Composer](https://getcomposer.org/)

#### Use

Can be used standalone or with Laravel 10, should auto register with Laravel 10.  If
not:

* add service provider to `config/app.php`

```
'providers' => [
...
  Smorken\R25\ServiceProvider::class,
```

Config options (.env)

R25_BASE_URI

R25_USER

R25_PASSWORD
