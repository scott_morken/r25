<?php

namespace Smorken\R25\Contracts;

use Smorken\R25\Contracts\Services\Base;
use Smorken\R25\Factory;

interface Connector
{
    /**
     * @param  null  $default
     */
    public function fromConfig(string $key, mixed $default = null): mixed;

    public function getFactory(): Factory;

    public function getRequester(): Request;

    public function getService(string $service): Base;
}
