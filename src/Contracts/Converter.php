<?php

namespace Smorken\R25\Contracts;

use Smorken\R25\Contracts\Models\Node;

interface Converter
{
    public function create(Xml $xml): Node;

    public function createFromArray(array $data): Node;

    public function getRaw(): mixed;

    public function toArray(): array;
}
