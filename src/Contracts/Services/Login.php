<?php

namespace Smorken\R25\Contracts\Services;

use Smorken\R25\Contracts\Converter;

interface Login extends Base
{
    public function login(string $username, string $password): bool;

    public function needsLogin(Converter $c): bool;
}
