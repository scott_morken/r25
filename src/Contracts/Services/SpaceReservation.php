<?php

namespace Smorken\R25\Contracts\Services;

use Smorken\R25\Contracts\Converter;

interface SpaceReservation extends Base
{
    public function getReservations(string $space_id, \DateTime|string $start, \DateTime|string $end): Converter;
}
