<?php

namespace Smorken\R25\Contracts;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

interface Request
{
    public function convert(Xml $xml): Converter;

    public function convertToXmlObject(ResponseInterface $response): Xml;

    public function createUrl(string $endpoint): string;

    public function getBaseUrl(): string;

    public function getClient(): ClientInterface;

    public function getConverter(): Converter;

    public function getXml(): Xml;

    public function request(string $method, string $uri, array $options = []): ResponseInterface;

    public function requestAndConvert(string $method, string $endpoint, array $options = []): Converter;

    public function setClient(ClientInterface $client): void;

    public function setConverter(Converter $converter): void;

    public function setXml(Xml $xml): void;
}
