<?php

namespace Smorken\R25\Contracts;

use Smorken\R25\Contracts\Models\Node;

interface Xml
{
    public function createXmlObject(string $xml_string): self;

    public function fromArray(array $data, object $xmlObj): self;

    public function getAttributes(?object $element, ?string $ns = null): array;

    public function getBase(): string;

    public function getChildren(object $element, ?string $ns = null): mixed;

    public function getImmediateAttributes(): mixed;

    public function getImmediateChildren(): mixed;

    public function getName(object $element): string;

    /**
     * @param  null  $value
     * @param  null  $attributes
     */
    public function getNode(string $name, ?string $value = null, ?array $attributes = null): Node;

    public function getXmlObject(): object;

    public function newXmlObject(?string $base = null, array $namespaces = []): object;

    public function setNodeClass(string $node_class): self;

    public function setXmlObject(object $xmlObj): self;

    public function testForError(object $xmlObj): self;

    public function toArray(object $xmlObj): array;

    public function toXml(): string;
}
