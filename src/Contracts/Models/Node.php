<?php

namespace Smorken\R25\Contracts\Models;

interface Node
{
    public function addAttribute(string $key, string $value): void;

    public function addChild(\Smorken\R25\Contracts\Models\Node $child): void;

    public function getAttribute(string $key): ?string;

    public function getAttributes(): array;

    /** @return \Smorken\R25\Contracts\Models\Node[]
     */
    public function getChildren(): array;

    public function getName(): string;

    public function getValue(): string;

    public function hasChildren(): bool;

    public function setAttributes(array $attributes): void;

    public function setChildren(array $children): void;

    public function setName(string $name): void;

    public function setValue(?string $value): void;

    public function toArray(bool $values_only = false): array;
}
