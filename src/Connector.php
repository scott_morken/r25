<?php

namespace Smorken\R25;

use Illuminate\Support\Arr;
use Smorken\R25\Contracts\Request;
use Smorken\R25\Contracts\Services\Base;

abstract class Connector implements \Smorken\R25\Contracts\Connector
{
    public function __construct(protected Factory $factory, protected Request $requester, protected array $config = []) {}

    public function fromConfig(string $key, mixed $default = null): mixed
    {
        return Arr::get($this->config, $key, $default);
    }

    public function getFactory(): Factory
    {
        return $this->factory;
    }

    public function getRequester(): Request
    {
        return $this->requester;
    }

    public function getService(string $service): Base
    {
        return $this->factory->create($service, $this->getRequester());
    }

    protected function getValue(string $key, array $item): ?string
    {
        if (isset($item['children'][$key])) {
            return $item['children'][$key]['value'];
        }

        return null;
    }
}
