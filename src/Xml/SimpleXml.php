<?php

namespace Smorken\R25\Xml;

use Smorken\R25\Contracts\Xml;
use Smorken\R25\Models\VO\Node;

class SimpleXml implements Xml
{
    protected string $default_namespace = 'r25';

    protected array $default_namespaces = [
        'r25' => 'http://www.collegenet.com/r25',
        'xl' => 'http://www.w3.org/1999/xlink',
    ];

    protected array $namespaces = [];

    protected string $nodeClass = Node::class;

    protected ?\SimpleXMLElement $xmlObj = null;

    public function __construct(?string $xml_string = null)
    {
        if ($xml_string !== null) {
            $this->createXmlObject($xml_string);
        }
    }

    public function createXmlObject(string $xml_string): self
    {
        $o = simplexml_load_string($this->translateString($xml_string));
        if ($o === false) {
            throw new Exception('Invalid XML', Exception::BADXML);
        }
        $this->namespaces = $o->getNamespaces(true);
        $this->testForError($o);
        $this->xmlObj = $o;

        return $this;
    }

    public function fromArray(array $data, object $xmlObj): self
    {
        foreach ($data as $node) {
            if (is_array($node)) {
                $this->fromArray($node, $xmlObj);
            } else {
                $child = $xmlObj->addChild($node->getName(), (string) $node);
                foreach ($node->getAttributes() as $k => $v) {
                    $child->addAttribute($k, $v);
                }
                $this->fromArray($node->getChildren(), $child);
            }
        }
        $this->xmlObj = $xmlObj;

        return $this;
    }

    public function getAttributes(?object $element, ?string $ns = null): array
    {
        $atts = [];
        if ($element === null) {
            $element = $this->xmlObj;
        }
        foreach ($element->attributes($ns, true) as $a => $v) {
            $key = ($ns ? "$ns:$a" : "$a");
            $atts[$key] = (string) $v;
        }

        return $atts;
    }

    public function getBase(): string
    {
        return $this->getName($this->xmlObj);
    }

    public function getChildren(object $element, ?string $ns = null): mixed
    {
        if ($ns) {
            return $element->children($this->namespaces[$ns]);
        }

        return $element->children();
    }

    public function getImmediateAttributes(): array
    {
        return $this->getAllAttributes($this->xmlObj);
    }

    public function getImmediateChildren(): mixed
    {
        foreach ($this->namespaces as $ns => $urn) {
            $c = $this->getChildren($this->xmlObj, $ns);
            if ($c) {
                return $c;
            }
        }

        return null;
    }

    public function getName(object $element): string
    {
        return $element->getName();
    }

    public function getNode(
        string $name,
        ?string $value = null,
        ?array $attributes = null
    ): \Smorken\R25\Contracts\Models\Node {
        return new $this->nodeClass($name, $value, $attributes);
    }

    public function getXmlObject(): object
    {
        return $this->xmlObj;
    }

    public function newXmlObject(?string $base = null, array $namespaces = []): object
    {
        if ($base === null) {
            $base = 'root';
        }
        if (! $namespaces) {
            $namespaces = $this->default_namespaces;
        }
        if ($this->default_namespace && ! str_contains($base, ':')) {
            $base = sprintf('%s:%s', $this->default_namespace, $base);
        }
        $base = sprintf('<?xml version="1.0"?><%s %s></%s>', $base, $this->namespacesToString($namespaces), $base);

        return new \SimpleXMLElement($base);
    }

    public function setNodeClass(string $node_class): self
    {
        $this->nodeClass = $node_class;

        return $this;
    }

    public function setXmlObject(object $xmlObj): self
    {
        $this->xmlObj = $xmlObj;

        return $this;
    }

    public function testForError(object $xmlObj): self
    {
        foreach ($this->namespaces as $ns => $urn) {
            $c = $this->getChildren($xmlObj, $ns);
            if ($c->getName() === 'error') {
                $error_message = (string) $c[0]->msg;
                $msg_id = $c[0]->msg_id;
                $error_code = defined('XmlException::'.$msg_id) ? constant('XmlException::'.$msg_id) : 0;
                throw new Exception($error_message, $error_code);
            }
        }

        return $this;
    }

    public function toArray(object $xmlObj): array
    {
        $arr = [];
        foreach ($this->namespaces as $n => $urn) {
            foreach ($this->getChildren($xmlObj, $n) as $r) {
                if (count($this->getChildren($r, $n)) === 0) {
                    $arr[$r->getName()] = $this->getNode($r->getName(), (string) $r, $this->addAttributesToArray($r));
                } else {
                    $node = $this->getNode($r->getName(), null, $this->addAttributesToArray($r));
                    $node->setChildren($this->toArray($r));
                    $arr[$r->getName()][] = $node;
                }
            }
        }

        return $arr;
    }

    public function toXml(): string
    {
        return $this->xmlObj->saveXML();
    }

    protected function addAttributesToArray(object $xmlObj): array
    {
        $arr = [];
        $attrs = $this->getAllAttributes($xmlObj);
        foreach ($attrs as $k => $v) {
            $arr[$k] = $v;
        }

        return $arr;
    }

    protected function getAllAttributes(?object $element): array
    {
        $attrs = [];
        foreach ($this->getAttributes($element, null) as $a => $v) {
            $attrs[$a] = (string) $v;
        }
        foreach ($this->namespaces as $n => $url) {
            $t = $this->getAttributes($element, $n);
            foreach ($t as $a => $v) {
                $attrs[$a] = (string) $v;
            }
        }

        return $attrs;
    }

    protected function namespacesToString(array $namespaces): ?string
    {
        if ($namespaces) {
            $t = [];
            foreach ($namespaces as $ns => $urn) {
                $t[] = sprintf('xmlns:%s="%s"', $ns, $urn);
            }

            return implode(' ', $t);
        }

        return null;
    }

    protected function translateString(string $xml_string): string
    {
        $trans_tbl = [
            chr(38) => '&amp;',
        ];

        return strtr($xml_string, $trans_tbl);
    }
}
