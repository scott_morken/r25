<?php

namespace Smorken\R25\Xml;

class Exception extends \Exception
{
    public const BADREQUEST = 1;

    public const BADXML = 99;

    public const UNAUTHORIZED = 2;

    public const UNKNOWN = 0;
}
