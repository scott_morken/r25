<?php

namespace Smorken\R25\Requesters;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Smorken\R25\Contracts\Converter;
use Smorken\R25\Contracts\Request;
use Smorken\R25\Contracts\Xml;

class Guzzle implements Request
{
    public function __construct(
        protected ClientInterface $client,
        protected Xml $xml,
        protected Converter $converter,
        protected array $client_config = []
    ) {}

    public function convert(Xml $xml): Converter
    {
        $this->getConverter()
            ->create($xml);

        return $this->getConverter();
    }

    public function convertToXmlObject(ResponseInterface $response): Xml
    {
        $this->getXml()
            ->createXmlObject((string) $response->getBody());

        return $this->getXml();
    }

    public function createUrl(string $endpoint): string
    {
        return rtrim($this->getBaseUrl(), '/').'/'.ltrim($endpoint, '/');
    }

    public function getBaseUrl(): string
    {
        if (array_key_exists('base_uri', $this->client_config)) {
            return $this->client_config['base_uri'];
        }

        return '';
    }

    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    public function setClient(ClientInterface $client): void
    {
        $this->client = $client;
    }

    public function getConverter(): Converter
    {
        return $this->converter;
    }

    public function setConverter(Converter $converter): void
    {
        $this->converter = $converter;
    }

    public function getXml(): Xml
    {
        return $this->xml;
    }

    public function setXml(Xml $xml): void
    {
        $this->xml = $xml;
    }

    public function request(string $method, string $uri, array $options = []): ResponseInterface
    {
        return $this->getClient()
            ->request($method, $uri, $options);
    }

    public function requestAndConvert(string $method, string $endpoint, array $options = []): Converter
    {
        $r = $this->request($method, $this->createUrl($endpoint), $options);
        $x = $this->convertToXmlObject($r);

        return $this->convert($x);
    }

    /**
     * Not used with Guzzle since it automatically throws an exception
     *
     * @throws Exception
     */
    protected function handleInvalidResponse($r): void
    {
        $err = match ($r->getStatusCode()) {
            401 => 'Unauthorized',
            403 => 'You do not have permission to view the requested resource',
            default => 'The server reported an error ['.$r->getStatusCode().']',
        };
        throw new Exception($err);
    }
}
