<?php

namespace Smorken\R25\Converters;

use Smorken\R25\Contracts\Converter;
use Smorken\R25\Contracts\Models\Node;
use Smorken\R25\Contracts\Xml;

class ToArray implements Converter
{
    protected array $data = [];

    public function __construct(protected array $options = []) {}

    public function create(Xml $xml): Node
    {
        $arr = $xml->toArray($xml->getXmlObject());

        return $this->createFromArray($arr);
    }

    public function createFromArray(array $data): Node
    {
        $this->data = $data;

        return new \Smorken\R25\Models\VO\Node('converted', null, $data);
    }

    public function getRaw(): mixed
    {
        return $this->data;
    }

    public function toArray(): array
    {
        return $this->internalToArray($this->data);
    }

    protected function internalToArray(array $data): array
    {
        $arr = [];
        foreach ($data as $k => $v) {
            if ($v instanceof Node) {
                $arr[$k] = $v->toArray(true);
            } elseif (is_array($v)) {
                $arr[$k] = $this->internalToArray($v);
            } else {
                $arr[$k] = $v;
            }
        }

        return $arr;
    }
}
