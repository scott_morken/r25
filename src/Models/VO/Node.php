<?php

namespace Smorken\R25\Models\VO;

class Node implements \Smorken\R25\Contracts\Models\Node, \Stringable
{
    protected array $children = [];

    public function __construct(protected string $name, protected ?string $value = null, protected array $attributes = []) {}

    public function __get(string $key): ?string
    {
        if (array_key_exists($key, $this->children) &&
            $this->children[$key] instanceof \Smorken\R25\Contracts\Models\Node) {
            return $this->children[$key]->getValue();
        }

        return null;
    }

    public function __set(string $key, ?string $value): void
    {
        if (array_key_exists($key, $this->children) &&
            $this->children[$key] instanceof \Smorken\R25\Contracts\Models\Node) {
            $this->children[$key]->setValue($value);
        }
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

    public function addAttribute(string $key, string $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function addChild(\Smorken\R25\Contracts\Models\Node $child): void
    {
        $this->children[] = $child;
    }

    public function encode(string $str): string
    {
        return htmlspecialchars($str, ENT_XML1, 'UTF-8');
    }

    public function getAttribute(string $key): ?string
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->encode($this->attributes[$key]);
        }

        return null;
    }

    public function getAttributes(): array
    {
        $atts = [];
        foreach ($this->attributes as $k => $v) {
            $atts[$k] = $this->encode($v);
        }

        return $atts;
    }

    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /** @return \Smorken\R25\Contracts\Models\Node[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getValue(): string
    {
        return $this->encode($this->value ?? '');
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function hasChildren(): bool
    {
        return count($this->children) > 0;
    }

    public function toArray(bool $values_only = false): array
    {
        $map_func = function ($v) use (&$map_func, $values_only) {
            if ($v instanceof \Smorken\R25\Contracts\Models\Node) {
                return $v->toArray($values_only);
            } else {
                return array_map($map_func, $v);
            }
        };
        if ($values_only) {
            return ['value' => $this->getValue(), 'children' => array_map($map_func, $this->getChildren())];
        }

        return [
            'value' => $this->getValue(),
            'attributes' => $this->getAttributes(),
            'children' => array_map($map_func, $this->getChildren()),
        ];
    }
}
