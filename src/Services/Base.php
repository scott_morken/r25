<?php

namespace Smorken\R25\Services;

use Smorken\R25\Contracts\Request;

abstract class Base
{
    protected string $endpoint;

    public function __construct(protected Request $handler) {}

    public function getEndpoint(): ?string
    {
        return $this->endpoint;
    }

    public function getHandler(): Request
    {
        return $this->handler;
    }
}
