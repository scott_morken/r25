<?php

namespace Smorken\R25\Services;

use Smorken\R25\Contracts\Converter;

class SpaceReservation extends Base implements \Smorken\R25\Contracts\Services\SpaceReservation
{
    protected string $endpoint = 'rm_reservations.xml';

    public function getReservations(string $space_id, \DateTime|string $start, \DateTime|string $end): Converter
    {
        $r = $this->getHandler()
            ->requestAndConvert('GET', $this->endpoint, [
                'query' => [
                    'space_id' => $space_id,
                    'start_dt' => $start,
                    'end_dt' => $end,
                ],
            ]);

        return $r;
    }
}
