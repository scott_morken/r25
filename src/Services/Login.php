<?php

namespace Smorken\R25\Services;

use Smorken\R25\Contracts\Converter;

class Login extends Base implements \Smorken\R25\Contracts\Services\Login
{
    protected string $endpoint = 'login.xml';

    public function getLoginResponse(): Converter
    {
        return $this->getHandler()
            ->requestAndConvert('GET', $this->endpoint);
    }

    public function login(string $username, string $password): bool
    {
        $r = $this->getLoginResponse();
        if ($this->needsLogin($r)) {
            if ($this->setLoginElements($username, $password, $r)) {
                $xml = $this->getHandler()
                    ->getXml();
                $this->getHandler()
                    ->getXml()
                    ->fromArray($r->getRaw(), $xml->newXmlObject('login_challenge'));
                $pr = $this->getHandler()
                    ->requestAndConvert('POST', $this->endpoint, [
                        'body' => $xml->toXml(),
                        'headers' => [
                            'Content-Type' => 'text/xml;charset=UTF-8',
                        ],
                    ]);

                return ! $this->needsLogin($pr);
            }

            return false;
        } else {
            return true;
        }
    }

    public function needsLogin(Converter $c): bool
    {
        $r = $c->getRaw();
        $needs_login = ! (isset($r['login'][0]) && $r['login'][0]->success === 'T');

        return $needs_login;
    }

    protected function createBasicResponse(string $username, string $password): string
    {
        return 'Basic '.base64_encode($username.':'.$password);
    }

    protected function createMd5Response(string $username, string $password, string $challenge): string
    {
        $pw = md5($password);
        $pw = $pw.':'.$challenge;
        $pw = md5($pw);

        return $pw;
    }

    protected function setLoginElements(string $username, string $password, Converter $c): bool
    {
        $r = $c->getRaw();
        if (isset($r['login'][0]) && $r['login'][0]->challenge) {
            $type = $r['login'][0]->challenge;
            if (str_starts_with($type, 'Basic')) {
                $response = $this->createBasicResponse($username, $password);
            } else {
                $response = $this->createMd5Response($username, $password, $type);
            }
            $r['login'][0]->username = $username;
            $r['login'][0]->response = $response;
            $r['login'][0]->challenge = null;

            return true;
        }

        return false;
    }
}
