<?php

namespace Smorken\R25;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Cookie\SessionCookieJar;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Foundation\Application;
use Smorken\R25\Contracts\Converter;
use Smorken\R25\Contracts\Request;
use Smorken\R25\Contracts\Xml;
use Smorken\R25\Converters\ToArray;
use Smorken\R25\Requesters\Guzzle;
use Smorken\R25\Xml\SimpleXml;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootConfig();
    }

    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->bindFactory();
        $this->bindConverter();
        $this->bindXml();
        $this->bindRequester();
    }

    protected function bindConverter(): void
    {
        $this->app->bind(Converter::class, fn (Application $app) => new ToArray);
    }

    protected function bindFactory(): void
    {
        $this->app->bind(Factory::class, fn (Application $app) => new Factory($app['config']->get('r25.services', [])));
    }

    protected function bindRequester(): void
    {
        $this->app->bind(Request::class, function (Application $app) {
            $client = $this->client($app);
            $xml = $app[Xml::class];
            $conv = $app[Converter::class];
            $conf = $app['config']->get('r25.client', []);

            return new Guzzle($client, $xml, $conv, $conf);
        });
    }

    protected function bindXml(): void
    {
        $this->app->bind(Xml::class, fn (Application $app) => new SimpleXml);
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'r25');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('r25.php')], 'r25-config');
    }

    protected function client(Application $app): ClientInterface
    {
        $cj = new SessionCookieJar('r25_cookiejar', true);
        $config = $this->app['config']->get('r25.client', []);
        $opts = $config['options'] ?? [];
        $opts['cookies'] = $cj;
        if ($config['test'] ?? false) {
            return $this->testClient($app, $opts);
        }

        return $this->liveClient($opts);
    }

    protected function createResponses(iterable $endpoints): array
    {
        $responses = [];
        foreach ($endpoints as $endpoint => $status_code) {
            $responses[] = new Response($status_code, [], $this->getXml($endpoint));
        }

        return $responses;
    }

    protected function getTestClient(string|array $endpoint, array $opts): ClientInterface
    {
        $mock = new MockHandler($this->createResponses($endpoint));
        $handler = HandlerStack::create($mock);

        return new Client(array_merge(['handler' => $handler], $opts));
    }

    protected function getXml(string $endpoint): string
    {
        $fp = __DIR__.'/../tests/data/'.$endpoint;

        return file_get_contents($fp);
    }

    protected function liveClient(array $opts): ClientInterface
    {
        return new Client($opts);
    }

    protected function testClient(Application $app, array $opts): ClientInterface
    {
        $responses = $app['config']->get('r25.test_responses', [
            'login_challenge.xml' => 200,
            'login_response.xml' => 200,
            'rm_reservations.121.xml' => 200,
            'rm_reservations.123.xml' => 200,
            'rm_reservations.126.xml' => 200,
            'rm_reservations.12276.xml' => 200,
        ]);

        return $this->getTestClient($responses, $opts);
    }
}
