<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 11/3/15
 * Time: 9:54 AM
 */

namespace Smorken\R25;

class Factory
{
    public function __construct(protected array $map = []) {}

    public function create(string $name): ?object
    {
        $args = func_get_args();
        array_shift($args);
        if ($this->has($name)) {
            $cls = $this->map[$name];
            if (is_object($cls)) {
                return $cls;
            }
            $obj = $this->createClassFromNameAndArgs($cls, $args);
            $this->map[$name] = $obj;

            return $obj;
        }

        return null;
    }

    public function has(string $name): bool
    {
        return array_key_exists($name, $this->map);
    }

    protected function createClassFromNameAndArgs(string $className, array $args): object
    {
        if (count($args) === 0) {
            return new $className;
        }
        $r = new \ReflectionClass($className);

        return $r->newInstanceArgs($args);
    }
}
